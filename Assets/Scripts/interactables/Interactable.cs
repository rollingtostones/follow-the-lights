﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour{
    public bool isFocus = false;
    public bool hasInteracted = false;
    public bool isInteractable;
    public Transform player;

    void Update(){
        if (isFocus == true && hasInteracted == false){
            Interact();
            hasInteracted = true;
        }
    }

    public void OnFocus(Transform who){
        isFocus = true;
        player = who;
        hasInteracted = false;

        if(isInteractable){
            ShowPanel();
            FocusCamera();
        }
    }

    public void LeaveFocus(){
        isFocus = false;
        player = null;

        if(isInteractable){
            HidePanel();
            RestoreCamera();
        }
    }

    public virtual void Interact(){}

    public virtual void ShowPanel(){}
    public virtual void HidePanel(){}

    public virtual void FocusCamera(){}
    public virtual void RestoreCamera(){}
}
