﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InteractableBuilding : Interactable{

    public Building building;
    public TextMeshProUGUI buildingName, buildingDescription;
    public GameObject panel, mainCamera, customCamera;

    void Update(){
        if(isFocus){
            if(Input.GetKeyDown(KeyCode.E)){
                EnterBuilding();
                HidePanel();
                RestoreCamera();
            }

            if((Input.GetKeyDown(KeyCode.Q)) || (Input.GetKeyDown(KeyCode.Escape))){
                HidePanel();
            }
        }
    }

    public override void ShowPanel(){
        panel.SetActive(true);
        buildingName.text = building.name;
        buildingDescription.text = building.description;
    }

    public override void HidePanel(){
        panel.SetActive(false);
    }

    public override void FocusCamera(){
        mainCamera.SetActive(false);
        customCamera.SetActive(true);
    }

    public override void RestoreCamera(){
        customCamera.SetActive(false);
        mainCamera.SetActive(true);
    }

    void EnterBuilding(){
        // transition and change of scene
    }
}
