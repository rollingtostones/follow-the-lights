﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class PickUp : Interactable{
    public Item item;
    public TextMeshProUGUI itemName;
    public GameObject panel, mainCamera, customCamera;

    void Update(){
        if(isFocus){
            if(Input.GetKeyDown(KeyCode.E)){
                PickUpItem(item);
                HidePanel();
                RestoreCamera();
            }

            if((Input.GetKeyDown(KeyCode.Q)) || (Input.GetKeyDown(KeyCode.Escape))){
                HidePanel();
            }
        }
    }

    public override void ShowPanel(){
        panel.SetActive(true);
        itemName.text = item.name;
    }

    public override void HidePanel(){
        panel.SetActive(false);
    }

    public override void FocusCamera(){
        mainCamera.SetActive(false);
        customCamera.SetActive(true);
    }

    public override void RestoreCamera(){
        customCamera.SetActive(false);
        mainCamera.SetActive(true);
    }

    public void PickUpItem(Item item){
        bool wasPickedUp = Inventory.instance.AddItem(item);
        if(wasPickedUp){
            Destroy(gameObject);
        }
    }
}