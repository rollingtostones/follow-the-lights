﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour{
    Inventory inventory;
    public Transform itemsParent;
    InventorySlot[] slots;

    void Start(){
        inventory = Inventory.instance;
        inventory.onItemChangedCallback += UpdateUI;
        slots = itemsParent.GetComponentsInChildren<InventorySlot>();
    }

    void UpdateUI(){
        for(int i = 0; i < slots.Length; i++){
            if(i < inventory.items.Count){
                slots[i].AddItemToSlot(inventory.items[i]);
            }else{
                slots[i].ClearSlot();
            }
        }
    }
}