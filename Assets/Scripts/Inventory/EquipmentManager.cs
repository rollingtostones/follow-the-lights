﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour{

    public static EquipmentManager instance;
    public Equipment[] currentEquipment;
    public int numSlots;
    public EquipmentMeshControl equipmentMeshControl;

    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
    public OnEquipmentChanged onEquipmentChanged;

    void Awake(){
        instance = this;
    }

    void Start(){
        numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currentEquipment = new Equipment[numSlots];
    }

    void ListCurrentEquipment(){
        string total = "Equipment: ";
        for(int i = 0; i < numSlots; i++){
            if(currentEquipment[i] != null){
                string temp = currentEquipment[i].name;
                total += temp + " - ";
            }
        }
    }

    public void Equip(Equipment newItem){
        Equipment oldItem = null;
        // item position
        int slotIndex = (int)newItem.equipSlot;
        // asign old item, if there is any
        if (currentEquipment[slotIndex] != null){
			oldItem = currentEquipment [slotIndex];
            equipmentMeshControl.deattachtemToPlayer(oldItem);
		}
        // equip
        currentEquipment [slotIndex] = newItem;
        if(onEquipmentChanged != null){
            onEquipmentChanged.Invoke(newItem, oldItem);
            equipmentMeshControl.attachItemToPlayer(newItem);
        }
    }

    public void UnEquip(Equipment oldItem){
        int slotIndex = (int)oldItem.equipSlot;
        currentEquipment[slotIndex] = null;

        if(onEquipmentChanged != null){
            onEquipmentChanged.Invoke(null, oldItem);
        }
        //remove attatchment
        equipmentMeshControl.deattachtemToPlayer(oldItem);
    }
}
