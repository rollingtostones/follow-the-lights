﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class InventorySlot : MonoBehaviour, ISelectHandler{
    
    Item item;
    public Image icon;
    public ItemDetails itemDetails;
    public GameObject buttonGameObject;
    public EquipmentManager currentEquipmentList;
    public TextMeshProUGUI isEquiped;

    void Start(){
        currentEquipmentList.onEquipmentChanged += UpdateCurrentEquipment;
    }

    void Update(){
        if (buttonGameObject == EventSystem.current.currentSelectedGameObject){
            if(item != null){
                if(Input.GetKeyUp(KeyCode.E)){
                    UseItem();
                }

                if(Input.GetKeyUp(KeyCode.R)){
                    DropItem();
                }
            }
        }
    }

    public void AddItemToSlot(Item newItem){
        item = newItem;
        icon.sprite = item.icon;
        icon.enabled = true;
    }

    public void ClearSlot(){
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        isEquiped.text = " ";
    }

    bool itemIsEquiped(Item item){
        Equipment[] equipmentList = currentEquipmentList.currentEquipment;
        bool isEquiped = false;

        for(int i = 0; i < equipmentList.Length; i++){
            if(equipmentList[i] != null){
                if(item == equipmentList[i]){
                    isEquiped = true;
                }
            }
        }
        return isEquiped;
    }

    void UpdateCurrentEquipment(Equipment newItem, Equipment oldItem){
        if(item != null){
            isEquiped.text = "";
            Equipment[] equipmentList = currentEquipmentList.currentEquipment;
            if(itemIsEquiped(item)){
                isEquiped.text = "equiped";
            }
        }
    }

    public void OnSelect(BaseEventData eventData){
        if(item != null){
            itemDetails.UpdateUI(item);
        }else{
            itemDetails.ClearItemDetails();
        }
    }

    public void UseItem(){
        if(item.isEquipment){
            if(itemIsEquiped(item)){
                UnEquipItem();
            }else{
                EquipItem();
            }
        }else{
            ConsumeItem();
        }
    }

    public void DropItem(){
        UnEquipItem();
        itemDetails.ClearItemDetails();
        Inventory.instance.RemoveItem(item);
    }

    public void EquipItem(){
        item.UseItem();
    }

    public void UnEquipItem(){
        item.UnEquipThisItem();
    }

    public void ConsumeItem(){
        item.UseItem();
    }
}