﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Equipment name", menuName = "Inventory/Equipment")]
public class Equipment : Item
{
    public EquipmentSlot equipSlot;

    public GameObject mesh;
    public Vector3 equipedPosition;
    public Quaternion equipedRotation;

    public GameObject objectInScene;

    public override void UseItem(){
        base.UseItem();
        EquipmentManager.instance.Equip(this);
    }

    public override void UnEquipThisItem(){
        EquipmentManager.instance.UnEquip(this);
    }
}

public enum EquipmentSlot {
    Weapon, Torch, Armor, Boots
}