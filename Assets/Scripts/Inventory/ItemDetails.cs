﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemDetails : MonoBehaviour{
    public TextMeshProUGUI itemTitle;
    public TextMeshProUGUI itemDescription;
    public TextMeshProUGUI itemType;
    public Image itemIcon;
    public GameObject instrucionsPanel;

    public TextMeshProUGUI statText;

    public GameObject attackIcon;
    public GameObject defenseIcon;
    public GameObject speedIcon;

    Item item;

    public void UpdateUI(Item itemOnFocus){
        instrucionsPanel.SetActive(true);
        
        itemTitle.enabled = true;
        itemTitle.text = itemOnFocus.name;

        itemIcon.enabled = true;
        itemIcon.sprite = itemOnFocus.icon;

        itemDescription.enabled = true;
        itemDescription.text = itemOnFocus.description;

        itemType.enabled = true;
        itemType.text = itemOnFocus.typeOfItem;

        if(itemOnFocus.isEquipment == true){
            if(itemOnFocus.attackModifier != 0){
                statText.text = itemOnFocus.attackModifier.ToString();
                attackIcon.SetActive(true);
                speedIcon.SetActive(false);
                defenseIcon.SetActive(false);
            }else if (itemOnFocus.defenseModifier != 0){
                statText.text = itemOnFocus.defenseModifier.ToString();
                defenseIcon.SetActive(true);
                speedIcon.SetActive(false);
                attackIcon.SetActive(false);
            }else if (itemOnFocus.speedModifier != 0){
                statText.text = itemOnFocus.speedModifier.ToString();
                speedIcon.SetActive(true);
                attackIcon.SetActive(false);
                defenseIcon.SetActive(false);
            }
        }else{
            statText.text = "";
            speedIcon.SetActive(false);
            attackIcon.SetActive(false);
            defenseIcon.SetActive(false);
        }
    }

    public void ClearItemDetails(){
        itemTitle.enabled = false;
        itemIcon.enabled = false;
        itemDescription.enabled = false;
        speedIcon.SetActive(false);
        attackIcon.SetActive(false);
        defenseIcon.SetActive(false);
        statText.text = "";

        instrucionsPanel.SetActive(false);
    }
}
