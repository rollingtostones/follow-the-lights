﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {
    public List<Item> items = new List<Item>();
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;
    public int space = 12;
    public static Inventory instance;

    void Awake(){
        if(instance != null){
            return;
        }
        instance = this;
    }

    public bool AddItem(Item item){
        if(!item.isDefaultItem){
            if(items.Count >= space){
                return false;
            }
            items.Add(item);
            if(onItemChangedCallback != null){
                onItemChangedCallback.Invoke();
            }
            return true;
        }
        return false;
    }

    public void RemoveItem(Item item){
        items.Remove(item);
        if(onItemChangedCallback != null){
            onItemChangedCallback.Invoke();
        }
    }
}
