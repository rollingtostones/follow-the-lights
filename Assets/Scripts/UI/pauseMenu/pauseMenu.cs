﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pauseMenu : MonoBehaviour{

    public void exitGame(){
        Application.Quit();
    }

    public void startScreen(){
        SceneManager.LoadScene (sceneName:"mainMenu");
    }
}
