﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EquipmentUI : MonoBehaviour{
    public EquipmentManager currentEquipmentList;
    public PlayerStats playerStats;
    public TextMeshProUGUI attackStats, torchStat, defenseStats, speedStats;
    public Image currentWeaponImage, currentTorchImage, currentShieldImage, currentBootsImage;
    public bool isMainUI;

    void Start(){
        currentEquipmentList.onEquipmentChanged += UpdateCurrentEquipment;
        if(isMainUI){
            disableObject(currentWeaponImage);
            disableObject(currentTorchImage);
        }else{
            disableObject(currentWeaponImage);
            disableObject(currentTorchImage);
            disableObject(currentShieldImage);
            disableObject(currentBootsImage);
        }
    }

    void UpdateCurrentEquipment(Equipment newItem, Equipment oldItem){
        if(isMainUI){
            showCroppedITemsInfo();
        }else{
            showFullItemsInfo();
            UpdatePlayerStats();
        }
    }

    public void UpdatePlayerStats(){
        attackStats.text = playerStats.attack.GetValue().ToString();
        defenseStats.text = playerStats.armor.GetValue().ToString();
        speedStats.text = playerStats.speed.GetValue().ToString();
    }

    void showCroppedITemsInfo(){
        if(currentEquipmentList.currentEquipment[0] == null){
            disableObject(currentWeaponImage);
        }else{
            enableObject(currentWeaponImage);
            currentWeaponImage.sprite = currentEquipmentList.currentEquipment[0].icon;
        }

        if(currentEquipmentList.currentEquipment[1] == null){
            disableObject(currentTorchImage);
        }else{
            enableObject(currentTorchImage);
            currentTorchImage.sprite = currentEquipmentList.currentEquipment[1].icon;
        }
    }

    void showFullItemsInfo(){
        if(currentEquipmentList.currentEquipment[0] == null){
            disableObject(currentWeaponImage);
        }else{
            enableObject(currentWeaponImage);
            currentWeaponImage.sprite = currentEquipmentList.currentEquipment[0].icon;
        }

        if(currentEquipmentList.currentEquipment[1] == null){
            disableObject(currentTorchImage);
        }else{
            enableObject(currentTorchImage);
            currentTorchImage.sprite = currentEquipmentList.currentEquipment[1].icon;
        }

        if(currentEquipmentList.currentEquipment[2] == null){
            disableObject(currentShieldImage);
        }else{
            enableObject(currentShieldImage);
            currentShieldImage.sprite = currentEquipmentList.currentEquipment[2].icon;
        }

        if(currentEquipmentList.currentEquipment[3] == null){
            disableObject(currentBootsImage);
        }else{
            enableObject(currentBootsImage);
            currentBootsImage.sprite = currentEquipmentList.currentEquipment[3].icon;
        }
    }

    void enableObject(Image itemImage){
        itemImage.enabled = true;
    }

    void disableObject(Image itemImage){
        itemImage.enabled = false;
    }
}