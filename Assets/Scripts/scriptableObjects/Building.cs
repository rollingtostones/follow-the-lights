﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "building name", menuName = "CustomObject/Building")]
public class Building : ScriptableObject{
    new public string name = "building name";
    public string description;
    public virtual void EnterBuilding(){}
}
