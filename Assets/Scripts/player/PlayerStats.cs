﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : CharacterStats{

    public LifeBar lifeBar;
    public EquipmentManager equipmentManager;

    void Start(){
        equipmentManager.onEquipmentChanged += OnEquipmentChanged;
        lifeBar.SetMaxValue(maxHealth);
    }

    void Update(){
        //ejemplo de daño
        if(Input.GetKeyDown(KeyCode.I)){
            TakeDamage(2);
        }

        if(Input.GetKeyDown(KeyCode.O)){
            Heal(2);
        }
    }

   public override void UpdateHealth(){
        lifeBar.SetcurrentHealth(currentHealth);
    }

    void OnEquipmentChanged(Equipment newItem, Equipment oldItem){
        if(newItem != null){
            armor.AddModifier(newItem.defenseModifier);
            attack.AddModifier(newItem.attackModifier);
            speed.AddModifier(newItem.speedModifier);
        }

        if(oldItem != null){
            armor.RemoveModifier(oldItem.defenseModifier);
            attack.RemoveModifier(oldItem.attackModifier);
            speed.RemoveModifier(oldItem.speedModifier);
        }
    }

    // void Dies(){
    //     uiManager.showGameOver();
    // }
}