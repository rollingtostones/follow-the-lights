﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Transform playerContainer;
    public Rigidbody rb;

    //movement
    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;
    [SerializeField] private float speed = 10f;
    [SerializeField] private float jumpForce = 10f;
    public Vector3 moveDirection;
    public float distToGround;

    //interaction
    public Interactable playerFocus;
    
    void Start(){
        rb = GetComponent<Rigidbody>();
        distToGround = GetComponent<Collider>().bounds.extents.y;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update() {
        if(Input.GetKeyUp(KeyCode.Space) & IsGrounded()){
            Jump();
        }

        float verticalMovement = Input.GetAxisRaw("Vertical");
        float horizontalMovement = Input.GetAxisRaw("Horizontal");
        moveDirection = verticalMovement * transform.forward + horizontalMovement * transform.right;

        float X = Input.GetAxis("Mouse X") * SmoothFactor;
        playerContainer.Rotate(0, X, 0);
    }

    private void FixedUpdate() {
        Move();
    }

    //movement control
    private void Move() {
        rb.MovePosition(transform.position + moveDirection.normalized * speed * Time.deltaTime);
    }

    void Jump(){
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }

    bool IsGrounded() {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + .1f);
    }

    //interactions
    private void OnTriggerEnter(Collider objectCollision){
        if(objectCollision.GetComponent<Interactable>()){
            Interactable interactable = objectCollision.GetComponent<Interactable>();
            SetFocus(interactable);
        }
    }

    void OnTriggerExit(Collider other){
        if(other.GetComponent<Interactable>()){
            Interactable otherInteractable = other.GetComponent<Interactable>();
            if(otherInteractable == playerFocus){
                RemoveFocus();
            }
        }
    }

    private void SetFocus(Interactable newFocus){
        if(playerFocus != newFocus){
            playerFocus = newFocus;
            newFocus.OnFocus(transform);
        }
    }

    private void RemoveFocus(){
        if(playerFocus != null){
            playerFocus.LeaveFocus();
            playerFocus = null;
        }
    }
}
